package com.example.andreea.loginapp.listViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.andreea.loginapp.R;
import com.example.andreea.loginapp.model.Recipe;

import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder> {
    private ClickListener clickListener;

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = inflater.inflate(R.layout.list_item_layout, viewGroup, false);
        return new RecipeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int i) {
        Recipe recipe = this.recipes.get(i);
        recipeViewHolder.mTitle.setText(recipe.getTitle());
        recipeViewHolder.mSecretIngridient.setText(recipe.getSecretIngridient());
        recipeViewHolder.mEstimatedPrice.setText(String.valueOf(recipe.getEstimatedPrice()));
        recipeViewHolder.mPreparation.setText(recipe.getPreparation());

    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (recipes != null) {
            return recipes.size();
        }
        return 0;
    }

    class RecipeViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mSecretIngridient;
        public TextView mEstimatedPrice;
        public TextView mPreparation;

        public FrameLayout cardView;

        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.txtTitle);
            mSecretIngridient = itemView.findViewById(R.id.txtSecretIngridient);
            mEstimatedPrice = itemView.findViewById(R.id.txtEstimatedPrice);
            mPreparation = itemView.findViewById(R.id.txtPreparation);
            cardView = itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.launchIntent(recipes.get(getAdapterPosition()));
                }
            });
        }
    }

    private final LayoutInflater inflater;
    private List<Recipe> recipes;

    public RecipeListAdapter(ClickListener clickListener, Context context) {
        this.clickListener = clickListener;
        inflater = LayoutInflater.from(context);
    }

    public interface ClickListener {
        void launchIntent(Recipe recipe);
    }

}
