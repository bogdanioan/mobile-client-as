package com.example.andreea.loginapp.listViews;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.andreea.loginapp.model.DeletedRecipe;
import com.example.andreea.loginapp.model.InsertedRecipe;
import com.example.andreea.loginapp.model.Recipe;
import com.example.andreea.loginapp.model.UpdatedRecipe;
import com.example.andreea.loginapp.repository.RecipeRepository;

import java.util.List;

public class RecipeViewModel extends AndroidViewModel {

    private RecipeRepository recipeRepository;
    private LiveData<List<Recipe>> recipes;

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        this.recipeRepository = new RecipeRepository(application);
        this.recipes = this.recipeRepository.getRecipes();
    }

    public LiveData<List<Recipe>> getAllRecipesByUserEmail(String userEmail) {
        return this.recipeRepository.getAllByUserEmail(userEmail);
    }

    public void deleteRecipe(Recipe recipe) {
        this.recipeRepository.delete(recipe);
    }

    public void deleteAllByUserEmail(String userEmail, boolean onlyOffline) {
        this.recipeRepository.deleteAllByUserEmail(userEmail, onlyOffline);
    }

    public LiveData<List<Recipe>> getAllRecipes() {
        return recipes;
    }

    public void update(Recipe recipe) {
        this.recipeRepository.update(recipe);
    }

    public void insert(Recipe recipe) {
        this.recipeRepository.insert(recipe);
    }

    public void deleteAll() {
        this.recipeRepository.deleteAll();
    }

    public void insertOffline(Recipe recipe) {
        this.recipeRepository.insertOffline(recipe);
    }

    public void deleteOffline(Recipe recipe) {
        this.recipeRepository.deleteOffline(recipe);
    }

    public void updateOfflie(Recipe recipe) {
        this.recipeRepository.updateOffline(recipe);
    }

    public List<InsertedRecipe> getOfflineInsertedByUserEmail(String email) {
        return this.recipeRepository.getOfflineInsertedByUserEmail(email);
    }

    public List<UpdatedRecipe> getOfflineUpdatedByUserEmail(String email) {
        return this.recipeRepository.getOfflineUpdatedByUserEmail(email);
    }

    public List<DeletedRecipe> getOfflineDeletedByUserEmail(String email) {
        return this.recipeRepository.getOfflineDeletedByUserEmail(email);
    }
}
