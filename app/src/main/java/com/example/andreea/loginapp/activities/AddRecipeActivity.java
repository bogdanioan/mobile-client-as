package com.example.andreea.loginapp.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.andreea.loginapp.R;
import com.example.andreea.loginapp.listViews.RecipeViewModel;
import com.example.andreea.loginapp.model.Recipe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddRecipeActivity extends AppCompatActivity {
    private EditText mTitle;
    private EditText mEstimatedPrice;
    private EditText mSecretIngridient;
    private EditText mPreparation;
    private View mProgress;
    private View mAddForm;

    private Recipe recipe = new Recipe();

    private boolean isAdd = true;

    private RecipeViewModel recipeViewModel;

    private AddRecipeTask mAddRecipeTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);
        mTitle = (EditText) findViewById(R.id.title);
        mEstimatedPrice = (EditText) findViewById(R.id.estimated_price);
        mSecretIngridient = (EditText) findViewById((R.id.secretIngridient));
        mPreparation = (EditText) findViewById(R.id.preparation);
        mProgress = findViewById(R.id.add_progress);
        mAddForm = findViewById(R.id.add_recipe_form);
        Button addButton = (Button) findViewById(R.id.add_recipe_button);
        addButton.setOnClickListener((view) -> {
            attemptAdd();
        });
        addButton.setBackgroundColor(Color.GREEN);

        FloatingActionButton backButton = (FloatingActionButton) findViewById(R.id.back);
        backButton.setOnClickListener((view)->{
            Intent intent = new Intent(AddRecipeActivity.this,RecipeListActivity.class);
            startActivity(intent);
        });
        this.recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);

        String recipe;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                recipe = null;
            } else {
                recipe = extras.getString("recipe");
                if (recipe != null)
                    isAdd = false;
            }
        } else {
            recipe = (String) savedInstanceState.getSerializable("recipe");
            if (recipe != null)
                isAdd = false;
        }

        handleEditMode(recipe);
    }

    private void handleEditMode(String recipe) {
        if (isAdd)
            return;
        else {
            try {
                JSONObject jsonObject = new JSONObject(recipe);
                this.recipe.setUuid(jsonObject.getString("uuid"));
                this.recipe.setTitle(jsonObject.getString("title"));
                this.recipe.setEstimatedPrice(jsonObject.getDouble("estimatedPrice"));
                this.recipe.setSecretIngridient(jsonObject.getString("secretIngridient"));
                this.recipe.setPreparation(jsonObject.getString("preparation"));
                this.recipe.setUserEmail(jsonObject.getString("userEmail"));
                mPreparation.setText(this.recipe.getPreparation());
                mTitle.setText(this.recipe.getTitle());
                mSecretIngridient.setText(this.recipe.getSecretIngridient());
                ;
                mEstimatedPrice.setText(String.valueOf(this.recipe.getEstimatedPrice()));
                Button deleteButton = (Button) findViewById(R.id.delete_recipe_button);
                deleteButton.setOnClickListener((view) -> {
                    attemptDelete();
                });
                deleteButton.setVisibility(View.VISIBLE);
                deleteButton.setBackgroundColor(Color.RED);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void attemptDelete() {
        showProgress(true);
        new DeleteRecipeTask(recipe).execute((Void) null);
    }

    private void attemptAdd() {
        if (mAddRecipeTask != null) {
            return;
        }

        // Reset errors.
        mTitle.setError(null);
        mPreparation.setError(null);

        // Store values at the time of the login attempt.
        String title = mTitle.getText().toString();
        String secretIngridient = mSecretIngridient.getText().toString();
        String price = mEstimatedPrice.getText().toString();
        double estimatedPrice = 0;
        if (!price.equals(""))
            estimatedPrice = Double.parseDouble(mEstimatedPrice.getText().toString());
        String preparation = mPreparation.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(title)) {
            mTitle.setError(getString(R.string.error_field_required));
            focusView = mTitle;
            cancel = true;
        } else if (TextUtils.isEmpty(preparation)) {
            mPreparation.setError(getString(R.string.error_field_required));
            focusView = mPreparation;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAddRecipeTask = new AddRecipeActivity.AddRecipeTask(title, secretIngridient, estimatedPrice, preparation);
            mAddRecipeTask.execute((Void) null);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mAddForm.setVisibility(show ? View.GONE : View.VISIBLE);
            mAddForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mAddForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgress.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            mAddForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public class AddRecipeTask extends AsyncTask<Void, Void, Boolean> {

        private final String mTitle;
        private final String mSecretIngridient;
        private final double mEstimatedPrice;
        private final String mPreparation;

        public AddRecipeTask(String mTitle, String mSecretIngridient, double mEstimatedPrice, String mPreparation) {
            this.mTitle = mTitle;
            this.mSecretIngridient = mSecretIngridient;
            this.mEstimatedPrice = mEstimatedPrice;
            this.mPreparation = mPreparation;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("title", mTitle);
                jsonObject.put("secretIngridient", mSecretIngridient);
                jsonObject.put("estimatedPrice", mEstimatedPrice);
                jsonObject.put("preparation", mPreparation);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Recipe newRecipe = new Recipe();
            if (isNetworkConnected()) {
                SharedPreferences prefs = AddRecipeActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
                String token = prefs.getString("token", "");
                if (token.isEmpty()) {
                    Intent intent = new Intent(AddRecipeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return null;
                }
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                if (isAdd) {
                    String URL = getResources().getString(R.string.api) + "/api/recipes/";
                    Request request = new Request.Builder()
                            .url(URL)
                            .post(requestBody)
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                    try {
                        Response response = client.newCall(request).execute();
                        if (response.code() != 200) {
                            Log.d("response:", "!=200");
                            return false;
                        }
                        JSONObject responseObject = new JSONObject(response.body().string());
                        newRecipe.setUuid(responseObject.getString("_id"));
                        newRecipe.setTitle(responseObject.getString("title"));
                        newRecipe.setEstimatedPrice(responseObject.getDouble("estimatedPrice"));
                        newRecipe.setSecretIngridient(responseObject.getString("secretIngridient"));
                        newRecipe.setPreparation(responseObject.getString("preparation"));
                        newRecipe.setUserEmail(responseObject.getString("userEmail"));
                        recipeViewModel.insert(newRecipe);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return false;
                    }
                    return true;
                } else {
                    String URL = getResources().getString(R.string.api) + "/api/recipes/" + recipe.getUuid();
                    Request request = new Request.Builder()
                            .url(URL)
                            .put(requestBody)
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                    try {
                        Response response = client.newCall(request).execute();
                        if (response.code() != 200) {
                            Log.d("response:", "!=200");
                            return false;
                        }
                        JSONObject responseObject = new JSONObject(response.body().string());
                        newRecipe.setUuid(recipe.getUuid());
                        newRecipe.setTitle(responseObject.getString("title"));
                        newRecipe.setEstimatedPrice(responseObject.getDouble("estimatedPrice"));
                        newRecipe.setSecretIngridient(responseObject.getString("secretIngridient"));
                        newRecipe.setPreparation(responseObject.getString("preparation"));
                        newRecipe.setUserEmail(responseObject.getString("userEmail"));
                        recipeViewModel.update(newRecipe);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return false;
                    }
                    return true;
                }
            } else {
                if (isAdd) {
                    SharedPreferences prefs = AddRecipeActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
                    String email = prefs.getString("email", "");
                    newRecipe.setUserEmail(email);
                    newRecipe.setPreparation(mPreparation);
                    newRecipe.setSecretIngridient(mSecretIngridient);
                    newRecipe.setEstimatedPrice(mEstimatedPrice);
                    newRecipe.setTitle(mTitle);
                    newRecipe.setUuid("draft" + UUID.randomUUID());
                    recipeViewModel.insertOffline(newRecipe);
                    return true;
                } else {
                    SharedPreferences prefs = AddRecipeActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
                    String email = prefs.getString("email", "");
                    newRecipe.setUuid(recipe.getUuid());
                    newRecipe.setUserEmail(email);
                    newRecipe.setPreparation(mPreparation);
                    newRecipe.setSecretIngridient(mSecretIngridient);
                    newRecipe.setEstimatedPrice(mEstimatedPrice);
                    newRecipe.setTitle(mTitle);
                    recipeViewModel.updateOfflie(newRecipe);
                    return true;
                }
            }
        }

        private boolean isNetworkConnected() {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivityManager.getActiveNetworkInfo() != null;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAddRecipeTask = null;
            showProgress(false);

            if (success) {
                Intent intent = new Intent(AddRecipeActivity.this, RecipeListActivity.class);
                startActivity(intent);
            }
        }


        @Override
        protected void onCancelled() {
            mAddRecipeTask = null;
            showProgress(false);
        }
    }

    public class DeleteRecipeTask extends AsyncTask<Void, Void, Boolean> {
        private final Recipe recipe;

        public DeleteRecipeTask(Recipe recipe) {
            this.recipe = recipe;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();
            if (isNetworkConnected()) {
                SharedPreferences prefs = AddRecipeActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
                String token = prefs.getString("token", "");
                if (token.isEmpty()) {
                    Intent intent = new Intent(AddRecipeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return null;
                }
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), "");
                String URL = getResources().getString(R.string.api) + "/api/recipes/" + recipe.getUuid();
                Request request = new Request.Builder()
                        .url(URL)
                        .delete(requestBody)
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.code() != 200) {
                        Log.d("response:", "!=200");
                        return false;
                    }
                    recipeViewModel.deleteRecipe(recipe);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            } else {
                recipeViewModel.deleteOffline(recipe);
                return true;
            }
        }

        private boolean isNetworkConnected() {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivityManager.getActiveNetworkInfo() != null;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAddRecipeTask = null;
            showProgress(false);

            if (success) {
                Intent intent = new Intent(AddRecipeActivity.this, RecipeListActivity.class);
                startActivity(intent);
            }
        }


        @Override
        protected void onCancelled() {
            mAddRecipeTask = null;
            showProgress(false);
        }
    }
}
