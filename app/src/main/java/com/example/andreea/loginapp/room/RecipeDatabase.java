package com.example.andreea.loginapp.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.andreea.loginapp.model.DeletedRecipe;
import com.example.andreea.loginapp.model.InsertedRecipe;
import com.example.andreea.loginapp.model.Recipe;
import com.example.andreea.loginapp.model.UpdatedRecipe;

@Database(entities = {Recipe.class,InsertedRecipe.class,UpdatedRecipe.class,DeletedRecipe.class}, version = 3, exportSchema = false)
public abstract class RecipeDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "kitchen_store_db";
    public static final String TABLE_NAME = "recipes";
    public static final String DELETED_RECIPES = "deleted_recipes";
    public static final String INSERTED_RECIPES = "inserted_recipes";
    public static final String UPDATED_RECIPES = "updated_recipes";
    private static volatile RecipeDatabase INSTANCE;

    public static RecipeDatabase getINSTANCE(final Context context){
        if(INSTANCE == null){
            synchronized (RecipeDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), RecipeDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract RecipeDAO RecipeDAO();

}
