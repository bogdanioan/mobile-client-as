package com.example.andreea.loginapp.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.andreea.loginapp.model.DeletedRecipe;
import com.example.andreea.loginapp.model.InsertedRecipe;
import com.example.andreea.loginapp.model.Recipe;
import com.example.andreea.loginapp.model.UpdatedRecipe;
import com.example.andreea.loginapp.room.RecipeDAO;
import com.example.andreea.loginapp.room.RecipeDatabase;

import java.util.List;

public class RecipeRepository {
    private RecipeDAO recipeDAO;
    private LiveData<List<Recipe>> recipes;

    public RecipeRepository(Application application) {
        recipeDAO = RecipeDatabase.getINSTANCE(application).RecipeDAO();
        recipes = recipeDAO.getAllRecipes();
    }

    public LiveData<List<Recipe>> getRecipes() {
        return recipes;
    }

    public LiveData<List<Recipe>> getAllByUserEmail(String userEmail) {
        return this.recipeDAO.getByUserEmail(userEmail);
    }

    //TO DO: async calls
    public void insertOffline(Recipe recipe) {
//        this.recipeDAO.insertRecipe(recipe);
//        this.recipeDAO.insertInsertedRecipe(new InsertedRecipe(recipe));
        new InsertOfflineAsyncTask(recipeDAO).execute(recipe);
    }

    public void updateOffline(Recipe recipe) {
//        this.recipeDAO.update(recipe);
//        List<InsertedRecipe> insertedRecipes = this.recipeDAO.getOfflineInsertedByUserEmail(recipe.getUserEmail());
//        boolean wasAddedOffline = false;
//        for (InsertedRecipe insertedRecipe : insertedRecipes) {
//            if (insertedRecipe.getUuid().equals(recipe.getUuid())) {
//                this.recipeDAO.deleteInsertedRecipe(insertedRecipe.getUuid());
//                this.recipeDAO.insertInsertedRecipe(new InsertedRecipe(recipe));
//                wasAddedOffline = true;
//            }
//        }
//        if (!wasAddedOffline) {
//            List<UpdatedRecipe> updatedRecipes = this.recipeDAO.getOfflineUpdatedByUserEmail(recipe.getUserEmail());
//            for (UpdatedRecipe updatedRecipe : updatedRecipes) {
//                if (updatedRecipe.getUuid().equals(recipe.getUuid())) {
//                    this.recipeDAO.deleteUpdatedRecipe(recipe.getUuid());
//                }
//            }
//            this.recipeDAO.insertUpdatedRecipe(new UpdatedRecipe(recipe));
//        }
        new UpdateOfflineAsyncTask(recipeDAO).execute(recipe);
    }

    public void deleteOffline(Recipe recipe) {
//        this.recipeDAO.deleteRecipe(recipe);
//        List<InsertedRecipe> insertedRecipes = this.recipeDAO.getOfflineInsertedByUserEmail(recipe.getUserEmail());
//        boolean wasAddedOffline = false;
//        for (InsertedRecipe insertedRecipe : insertedRecipes) {
//            if (insertedRecipe.getUuid().equals(recipe.getUuid())) {
//                this.recipeDAO.deleteInsertedRecipe(insertedRecipe.getUuid());
//                wasAddedOffline = true;
//            }
//        }
//        if (!wasAddedOffline) {
//            List<UpdatedRecipe> updatedRecipes = this.recipeDAO.getOfflineUpdatedByUserEmail(recipe.getUserEmail());
//            for (UpdatedRecipe updatedRecipe : updatedRecipes) {
//                if (updatedRecipe.getUuid().equals(recipe.getUuid())) {
//                    this.recipeDAO.deleteUpdatedRecipe(recipe.getUuid());
//                }
//            }
//            this.recipeDAO.insertDeletedRecipe(new DeletedRecipe(recipe));
//        }// else nothing to do
        new DeleteOfflineAsyncTask(recipeDAO).execute(recipe);
    }

    public List<InsertedRecipe> getOfflineInsertedByUserEmail(String email) {
        return this.recipeDAO.getOfflineInsertedByUserEmail(email);
    }

    public List<UpdatedRecipe> getOfflineUpdatedByUserEmail(String email) {
        return this.recipeDAO.getOfflineUpdatedByUserEmail(email);
    }

    public List<DeletedRecipe> getOfflineDeletedByUserEmail(String email) {
        return this.recipeDAO.getOfflineDeletedByUserEmail(email);
    }

    public void delete(Recipe recipe) {
        this.recipeDAO.deleteRecipe(recipe);
    }

    public void deleteAllByUserEmail(String userEmail, boolean onlyOffline) {
        new DeleteAsyncTask(this.recipeDAO, userEmail,onlyOffline).execute();
    }

    public void update(Recipe recipe) {
        new UpdateAsyncTask(this.recipeDAO).execute(recipe);
    }

    public void insert(Recipe recipe) {
        new InsertAsyncTask(this.recipeDAO).execute(recipe);
    }

    public void deleteAll() {
        new DeleteAsyncTask(this.recipeDAO, null,false).execute();
    }

    private static class InsertAsyncTask extends AsyncTask<Recipe, Void, Void> {

        private RecipeDAO recipeDAO;

        public InsertAsyncTask(RecipeDAO recipeDAO) {
            this.recipeDAO = recipeDAO;
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            this.recipeDAO.insertRecipe(recipes[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Recipe, Void, Void> {

        private RecipeDAO recipeDAO;

        public UpdateAsyncTask(RecipeDAO recipeDAO) {
            this.recipeDAO = recipeDAO;
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            this.recipeDAO.update(recipes[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private RecipeDAO recipeDAO;
        private String userEmail;
        private boolean onlyOffline;

        public DeleteAsyncTask(RecipeDAO recipeDAO, String userEmail, boolean onlyOffline) {
            this.recipeDAO = recipeDAO;
            this.onlyOffline = onlyOffline;
            this.userEmail = userEmail;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (userEmail == null) {
                if (!onlyOffline)
                    this.recipeDAO.deleteAllRecipes();
                this.recipeDAO.deleteAllDeletedRecipes();
                this.recipeDAO.deleteAllInsertedRecipes();
                this.recipeDAO.deleteAllUpdatedRecipes();
            } else {
                if (!onlyOffline)
                    this.recipeDAO.deleteAllByUserEmail(userEmail);
                this.recipeDAO.deleteAllInsertedRecipesByUserEmail(userEmail);
                this.recipeDAO.deleteAllUpdatedRecipesByUserEmail(userEmail);
                this.recipeDAO.deleteAllDeletedRecipesByUserEmail(userEmail);
            }
            return null;
        }
    }


    private static class InsertOfflineAsyncTask extends AsyncTask<Recipe, Void, Void> {

        private RecipeDAO recipeDAO;

        public InsertOfflineAsyncTask(RecipeDAO recipeDAO) {
            this.recipeDAO = recipeDAO;
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            this.recipeDAO.insertRecipe(recipes[0]);
            this.recipeDAO.insertInsertedRecipe(new InsertedRecipe(recipes[0]));
            return null;
        }
    }

    private static class UpdateOfflineAsyncTask extends AsyncTask<Recipe, Void, Void> {

        private RecipeDAO recipeDAO;

        public UpdateOfflineAsyncTask(RecipeDAO recipeDAO) {
            this.recipeDAO = recipeDAO;
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            Recipe recipe = recipes[0];
            this.recipeDAO.update(recipe);
            List<InsertedRecipe> insertedRecipes = this.recipeDAO.getOfflineInsertedByUserEmail(recipe.getUserEmail());
            boolean wasAddedOffline = false;
            for (InsertedRecipe insertedRecipe : insertedRecipes) {
                if (insertedRecipe.getUuid().equals(recipe.getUuid())) {
                    this.recipeDAO.deleteInsertedRecipe(insertedRecipe.getUuid());
                    this.recipeDAO.insertInsertedRecipe(new InsertedRecipe(recipe));
                    wasAddedOffline = true;
                }
            }
            if (!wasAddedOffline) {
                List<UpdatedRecipe> updatedRecipes = this.recipeDAO.getOfflineUpdatedByUserEmail(recipe.getUserEmail());
                for (UpdatedRecipe updatedRecipe : updatedRecipes) {
                    if (updatedRecipe.getUuid().equals(recipe.getUuid())) {
                        this.recipeDAO.deleteUpdatedRecipe(recipe.getUuid());
                    }
                }
                this.recipeDAO.insertUpdatedRecipe(new UpdatedRecipe(recipe));
            }
            return null;
        }
    }

    private static class DeleteOfflineAsyncTask extends AsyncTask<Recipe, Void, Void> {

        private RecipeDAO recipeDAO;

        public DeleteOfflineAsyncTask(RecipeDAO recipeDAO) {
            this.recipeDAO = recipeDAO;
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            Recipe recipe = recipes[0];
            this.recipeDAO.deleteRecipe(recipe);
            List<InsertedRecipe> insertedRecipes = this.recipeDAO.getOfflineInsertedByUserEmail(recipe.getUserEmail());
            boolean wasAddedOffline = false;
            for (InsertedRecipe insertedRecipe : insertedRecipes) {
                if (insertedRecipe.getUuid().equals(recipe.getUuid())) {
                    this.recipeDAO.deleteInsertedRecipe(insertedRecipe.getUuid());
                    wasAddedOffline = true;
                }
            }
            if (!wasAddedOffline) {
                List<UpdatedRecipe> updatedRecipes = this.recipeDAO.getOfflineUpdatedByUserEmail(recipe.getUserEmail());
                for (UpdatedRecipe updatedRecipe : updatedRecipes) {
                    if (updatedRecipe.getUuid().equals(recipe.getUuid())) {
                        this.recipeDAO.deleteUpdatedRecipe(recipe.getUuid());
                    }
                }
                this.recipeDAO.insertDeletedRecipe(new DeletedRecipe(recipe));
            }// else nothing to do
            return null;
        }

    }
}
