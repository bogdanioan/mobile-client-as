package com.example.andreea.loginapp.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.example.andreea.loginapp.room.RecipeDatabase;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = RecipeDatabase.TABLE_NAME)
public class Recipe {
    @PrimaryKey
    @NotNull
    private String uuid;
    private String title;
    private String secretIngridient;
    private double estimatedPrice;
    private String preparation;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    private String userEmail;

    public Recipe() {
    }

    public Recipe(@NotNull String uuid, String title, String secretIngridient, double estimatedPrice, String preparation, String userEmail) {
        this.uuid = uuid;
        this.title = title;
        this.secretIngridient = secretIngridient;
        this.estimatedPrice = estimatedPrice;
        this.preparation = preparation;
        this.userEmail = userEmail;
    }

    @NotNull
    public String getUuid() {
        return uuid;
    }

    public void setUuid(@NotNull String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSecretIngridient() {
        return secretIngridient;
    }

    public void setSecretIngridient(String secretIngridient) {
        this.secretIngridient = secretIngridient;
    }

    public double getEstimatedPrice() {
        return estimatedPrice;
    }

    public void setEstimatedPrice(double estimatedPrice) {
        this.estimatedPrice = estimatedPrice;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
}
