package com.example.andreea.loginapp.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.andreea.loginapp.model.DeletedRecipe;
import com.example.andreea.loginapp.model.InsertedRecipe;
import com.example.andreea.loginapp.model.Recipe;
import com.example.andreea.loginapp.model.UpdatedRecipe;

import java.util.List;

@Dao
public interface RecipeDAO {

    @Insert
    long insertRecipe(Recipe recipe);

    @Insert
    long[] insertRecipe(List<Recipe> recipes);

    @Query("SELECT * FROM " + RecipeDatabase.TABLE_NAME)
    LiveData<List<Recipe>> getAllRecipes();

    @Delete
    int deleteRecipe(Recipe recipe);

    @Insert
    long insertInsertedRecipe(InsertedRecipe insertedRecipe);

    @Insert
    long insertUpdatedRecipe(UpdatedRecipe updatedRecipe);

    @Insert
    long insertDeletedRecipe(DeletedRecipe deletedRecipe);

    @Query("DELETE FROM " + RecipeDatabase.INSERTED_RECIPES + " WHERE uuid = :uuid")
    void deleteInsertedRecipe(String uuid);

    @Query("DELETE FROM " + RecipeDatabase.TABLE_NAME)
    void deleteAllRecipes();

    @Query("DELETE FROM " + RecipeDatabase.INSERTED_RECIPES)
    void deleteAllInsertedRecipes();

    @Query("DELETE FROM " + RecipeDatabase.UPDATED_RECIPES)
    void deleteAllUpdatedRecipes();

    @Query("DELETE FROM " + RecipeDatabase.DELETED_RECIPES)
    void deleteAllDeletedRecipes();

    @Query("DELETE FROM " + RecipeDatabase.TABLE_NAME + " WHERE userEmail = :email")
    void deleteAllByUserEmail(String email);

    @Query("SELECT * FROM " + RecipeDatabase.TABLE_NAME + " WHERE userEmail = :email")
    LiveData<List<Recipe>> getByUserEmail(String email);

    @Query("SELECT * FROM " + RecipeDatabase.INSERTED_RECIPES + " WHERE userEmail = :email")
    List<InsertedRecipe> getOfflineInsertedByUserEmail(String email);

    @Query("SELECT * FROM " + RecipeDatabase.UPDATED_RECIPES + " WHERE userEmail = :email")
    List<UpdatedRecipe> getOfflineUpdatedByUserEmail(String email);

    @Query("SELECT * FROM " + RecipeDatabase.DELETED_RECIPES + " WHERE userEmail = :email")
    List<DeletedRecipe> getOfflineDeletedByUserEmail(String email);


    @Update
    public void update(Recipe recipe);

    @Query("DELETE FROM " + RecipeDatabase.UPDATED_RECIPES + " WHERE uuid = :uuid")
    void deleteUpdatedRecipe(String uuid);

    @Query("DELETE FROM " + RecipeDatabase.INSERTED_RECIPES + " WHERE userEmail = :email")
    void deleteAllInsertedRecipesByUserEmail(String email);

    @Query("DELETE FROM " + RecipeDatabase.UPDATED_RECIPES + " WHERE userEmail = :email")
    void deleteAllUpdatedRecipesByUserEmail(String email);

    @Query("DELETE FROM "+RecipeDatabase.DELETED_RECIPES+" WHERE userEmail = :email")
    void deleteAllDeletedRecipesByUserEmail(String email);
}
