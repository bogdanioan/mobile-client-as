package com.example.andreea.loginapp.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.andreea.loginapp.R;
import com.example.andreea.loginapp.listViews.EndlessRecyclerViewScrollListener;
import com.example.andreea.loginapp.listViews.RecipeListAdapter;
import com.example.andreea.loginapp.listViews.RecipeViewModel;
import com.example.andreea.loginapp.model.DeletedRecipe;
import com.example.andreea.loginapp.model.InsertedRecipe;
import com.example.andreea.loginapp.model.Recipe;
import com.example.andreea.loginapp.model.UpdatedRecipe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RecipeListActivity extends AppCompatActivity implements RecipeListAdapter.ClickListener {

    private RecyclerView recyclerView;
    private RecipeListAdapter recipeListAdapter;
    private LinearLayoutManager layoutManager;
    private String currentUserEmail;
    private RecipeViewModel recipeViewModel;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);
        SharedPreferences sharedPreferences = RecipeListActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
        this.currentUserEmail = sharedPreferences.getString("email", "");
        recipeListAdapter = new RecipeListAdapter(this, this);
        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);

        recipeViewModel.getAllRecipesByUserEmail(currentUserEmail).observe(this, (recipes) -> {
            recipeListAdapter.setRecipes(recipes);
        });

        recyclerView = (RecyclerView) findViewById(R.id.list_items);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recipeListAdapter);
        // Retain an instance so that you can call `resetState()` for fresh searches
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                loadNextDataFromApi(page);
            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);

        FloatingActionButton mAddRecipe = (FloatingActionButton) findViewById(R.id.insertRecipe);
        mAddRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecipeListActivity.this, AddRecipeActivity.class);
                startActivity(intent);

            }
        });

        FloatingActionButton logout = (FloatingActionButton) findViewById(R.id.logout);
        logout.setOnClickListener((view) -> {
            SharedPreferences prefs = RecipeListActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
            prefs.edit().remove("token").apply();
            prefs.edit().remove("email").apply();
            Intent intent = new Intent(RecipeListActivity.this, LoginActivity.class);
            startActivity(intent);
        });

        if (isNetworkConnected()) {
            try {
                new InsertDataTask(recipeViewModel).execute().get();
                new UpdateDataTask(recipeViewModel).execute().get();
                new DeleteDataTask(recipeViewModel).execute().get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new FetchDataTask(0, 2, recipeViewModel).execute();

        }
    }

    private void loadNextDataFromApi(int page) {
        Log.d("##############PAGE", String.valueOf(page));
        new FetchDataTask(page, 2, recipeViewModel).execute();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }

    @Override
    public void launchIntent(Recipe recipe) {
        Intent intent = new Intent(RecipeListActivity.this, AddRecipeActivity.class);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uuid", recipe.getUuid());
            jsonObject.put("title", recipe.getTitle());
            jsonObject.put("preparation", recipe.getPreparation());
            jsonObject.put("estimatedPrice", recipe.getEstimatedPrice());
            jsonObject.put("secretIngridient", recipe.getSecretIngridient());
            jsonObject.put("userEmail", recipe.getUserEmail());
            String recipeAsString = jsonObject.toString();
            intent.putExtra("recipe", recipeAsString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(intent);
    }

    public class FetchDataTask extends AsyncTask<Void, Void, Void> {
        private int page;
        private int count;
        private RecipeViewModel recipeViewModel;

        public FetchDataTask(int page, int count, RecipeViewModel recipeViewModel) {
            this.page = page;
            this.count = count;
            this.recipeViewModel = recipeViewModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            SharedPreferences prefs = RecipeListActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
            String token = prefs.getString("token", "");
            if (token.isEmpty()) {
                Intent intent = new Intent(RecipeListActivity.this, LoginActivity.class);
                startActivity(intent);
                return null;
            }
            boolean onlyOffline = page > 0;
            this.recipeViewModel.deleteAllByUserEmail(currentUserEmail, onlyOffline);
            String url = getString(R.string.api) + "/api/recipes" + "?page=" + page + "&count=" + count;
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.code() != 200) {
                    return null;
                }
                JSONArray jsonArray = new JSONArray(response.body().string());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Recipe recipe = new Recipe();
                    recipe.setUuid(jsonObject.getString("_id"));
                    recipe.setTitle(jsonObject.getString("title"));
                    recipe.setEstimatedPrice(jsonObject.getDouble("estimatedPrice"));
                    recipe.setSecretIngridient(jsonObject.getString("secretIngridient"));
                    recipe.setPreparation(jsonObject.getString("preparation"));
                    recipe.setUserEmail(jsonObject.getString("userEmail"));
                    recipeViewModel.insert(recipe);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class InsertDataTask extends AsyncTask<Void, Void, Void> {

        private RecipeViewModel recipeViewModel;

        public InsertDataTask(RecipeViewModel recipeViewModel) {
            this.recipeViewModel = recipeViewModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            SharedPreferences prefs = RecipeListActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
            String token = prefs.getString("token", "");
            if (token.isEmpty()) {
                Intent intent = new Intent(RecipeListActivity.this, LoginActivity.class);
                startActivity(intent);
                return null;
            }
            List<InsertedRecipe> draftedRecipes = recipeViewModel.getOfflineInsertedByUserEmail(currentUserEmail);
            Log.d("######ADDED", String.valueOf(draftedRecipes.size()));
            String url = getString(R.string.api) + "/api/recipes/";
            for (InsertedRecipe recipe : draftedRecipes) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("title", recipe.getTitle());
                    jsonObject.put("secretIngridient", recipe.getSecretIngridient());
                    jsonObject.put("estimatedPrice", recipe.getEstimatedPrice());
                    jsonObject.put("preparation", recipe.getPreparation());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                Request request = new Request.Builder()
                        .url(url)
                        .post(requestBody)
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.code() != 200) {
                        return null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public class UpdateDataTask extends AsyncTask<Void, Void, Void> {

        private RecipeViewModel recipeViewModel;

        public UpdateDataTask(RecipeViewModel recipeViewModel) {
            this.recipeViewModel = recipeViewModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            SharedPreferences prefs = RecipeListActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
            String token = prefs.getString("token", "");
            if (token.isEmpty()) {
                Intent intent = new Intent(RecipeListActivity.this, LoginActivity.class);
                startActivity(intent);
                return null;
            }
            List<UpdatedRecipe> updatedRecipes = recipeViewModel.getOfflineUpdatedByUserEmail(currentUserEmail);
            Log.d("######UPDATED", String.valueOf(updatedRecipes.size()));
            String url = getString(R.string.api) + "/api/recipes/";
            for (UpdatedRecipe recipe : updatedRecipes) {
                String putUrl = url + recipe.getUuid();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("uuid", recipe.getUuid());
                    jsonObject.put("title", recipe.getTitle());
                    jsonObject.put("secretIngridient", recipe.getSecretIngridient());
                    jsonObject.put("estimatedPrice", recipe.getEstimatedPrice());
                    jsonObject.put("preparation", recipe.getPreparation());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                Request request = new Request.Builder()
                        .url(putUrl)
                        .put(requestBody)
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.code() != 200) {
                        return null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }


    public class DeleteDataTask extends AsyncTask<Void, Void, Void> {
        private RecipeViewModel recipeViewModel;

        public DeleteDataTask(RecipeViewModel recipeViewModel) {
            this.recipeViewModel = recipeViewModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            SharedPreferences prefs = RecipeListActivity.this.getSharedPreferences(getString(R.string.user_data_file), Context.MODE_PRIVATE);
            String token = prefs.getString("token", "");
            if (token.isEmpty()) {
                Intent intent = new Intent(RecipeListActivity.this, LoginActivity.class);
                startActivity(intent);
                return null;
            }
            List<DeletedRecipe> deletedRecipes = recipeViewModel.getOfflineDeletedByUserEmail(currentUserEmail);
            Log.d("######DELETED", String.valueOf(deletedRecipes.size()));
            String url = getString(R.string.api) + "/api/recipes/";
            for (DeletedRecipe recipe : deletedRecipes) {
                String deleteUrl = url + recipe.getUuid();
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), "");
                Request request = new Request.Builder()
                        .url(deleteUrl)
                        .delete(requestBody)
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.code() != 200) {
                        return null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
