package com.example.andreea.loginapp.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.example.andreea.loginapp.room.RecipeDatabase;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = RecipeDatabase.DELETED_RECIPES)
public class DeletedRecipe {
    @PrimaryKey
    @NotNull
    private String uuid;
    private String userEmail;

    public DeletedRecipe(@NotNull String uuid, String userEmail) {
        this.uuid = uuid;
        this.userEmail = userEmail;
    }

    public DeletedRecipe() {
    }
    public DeletedRecipe(Recipe recipe) {
        this.uuid = recipe.getUuid();
        this.userEmail = recipe.getUserEmail();
    }

    @NotNull
    public String getUuid() {
        return uuid;
    }

    public void setUuid(@NotNull String uuid) {
        this.uuid = uuid;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
